# Idea Java文件基本设置

## 1. 文件头注释设置

### 1.1 配置路径
> File | Settings | Editor | File and Code Templates

### 1.2 配置选项
> `Includes --> File Header `

### 1.3 配置内容
> ```velocity
> /**
>   TODO
> 
>   <p> desc </p>
> 
> @author： #if($Author_Name !="")
>             ${Author_Name}
>          #else
>              ${USER}
>          #end
> 
> @date： ${DATE} ${TIME}
> */
> ```

## 2. Copyright (版权信息)设置

### 2.1 Scopes配置路径
> File | Settings | Appearance & Behavior | Scopes

### 2.2 Scopes配置选项
> `Add Scope --> Shared `

<img src="./img/scope-01.jpg" style="zoom:80%;" />

### 2.3 Scopes配置内容
<img src="./img/scope-02.jpg" style="zoom:80%;" />

### 2.4 Copyright配置路径
> File | Settings | Editor | Copyright | Copyright Profiles

### 2.5 Copyright配置选项
> ```Add Copyright Profile```

<img src="./img/copyright-01.jpg" style="zoom:80%;" />

### 2.6 Copyright配置内容
<img src="./img/copyright-02.jpg" style="zoom:80%;" />

```velocity
#set($copyrightBegin =${today.year})
#set($copyrightEnd =${today.year}+5)
Copyright (c) ${copyrightBegin}-${copyrightEnd} huangyuxi.com Authors. All Rights Reserved.
@Project: $project.name
@Module: $module.name
@Date: $today.format("yyyy-MM-dd 'T' HH:mm:ss")
```

### 2.7 Scope 与Copyright关联配置
<img src="./img/copyright-03.jpg" style="zoom:80%;" />
<img src="./img/copyright-04.jpg" style="zoom:80%;" />

## 3. Method头注释设置

### 3.1 配置路径

> File | Settings | Editor | Live Templates

### 3.2 配置选项

> * `Add --> Templates Group `
>
>   <img src="./img/method-01.jpg" style="zoom:80%;" />
>
> 
>
> * 我这里命名为 `hyx-group`，快捷键为 `mc`
> <img src="./img/method-02.jpg" style="zoom:80%;" />
> 模板内容不太好编辑，请将3.3配置内容复制进来再编辑修改，去掉第一行的 `/*`
>
> 
>
> * 模板中的变量如图设置
> <img src="./img/method-03.jpg" style="zoom:80%;" />

其中param 内容如下：	
```groovy
groovyScript("def result=''; def params=\"${_1}\".replaceAll('[\\\\[|\\\\]|\\\\s]', '').split(',').toList(); for(i = 0; i < params.size(); i++) {if(params[i] == '') return result;if(i==0) result += '\\n'; result+='	 * @param ' + params[i] + ((i < params.size() - 1) ? '\\n' : '')}; return result", methodParameters())
```

### 3.3 配置内容
```groovy
 /**
  * $method$ //TODO
  * $param$
  * @return $return$ //TODO
  * @author $user$
  * @date $date$ $time$
  **/
```
### 3.4  快速加入函数头注释

在一个函数上方输入 `/*mc`，然后按 `tab`键自动补全注释