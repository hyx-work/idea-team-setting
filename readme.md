>
> 不要错过的任何机会
> [二十年嵌入式老兵做Java](https://gitee.com/hyx-work/IotoJava)
>
>

# IDEA 操作

[java文件基本设置](idea-javafile-basesetting/idea-java-file-setting.md)

* [文件头注释设置](idea-javafile-basesetting/idea-java-file-setting.md)
* [Copyright (版权信息)设置](idea-javafile-basesetting/idea-java-file-setting.md)
* [Method头注释设置](idea-javafile-basesetting/idea-java-file-setting.md)

